plugins {
  application
}

application {
  mainClassName = "com.gitlab.security_products.tests.App"
}

repositories {
    mavenCentral()
}

dependencies {
    testCompile("junit:junit:4.12")
    compile("io.netty:netty:3.9.1.Final")
    compile("org.apache.maven:maven-artifact:3.3.9")
    compile("com.fasterxml.jackson.core:jackson-databind:2.9.2")
    compile("org.mozilla:rhino:1.7.10")
    compile("org.apache.geode:geode-core:1.1.1")
}
