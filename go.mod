module gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5

require (
	github.com/BurntSushi/toml v1.3.2
	github.com/google/uuid v1.6.0
	github.com/sirupsen/logrus v1.9.3
	github.com/spiegel-im-spiegel/go-cvss v1.0.0
	github.com/stretchr/testify v1.9.0
	github.com/umisama/go-cvss v0.0.0-20150430082624-a4ad666ead9b
	github.com/urfave/cli/v2 v2.27.2
	gitlab.com/gitlab-org/security-products/analyzers/command/v2 v2.4.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.3
	gitlab.com/gitlab-org/security-products/analyzers/report/v5 v5.0.0
	golang.org/x/exp v0.0.0-20240416160154-fe59bbe5cc7f
	gonum.org/v1/gonum v0.13.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	dario.cat/mergo v1.0.0 // indirect
	github.com/Microsoft/go-winio v0.6.1 // indirect
	github.com/ProtonMail/go-crypto v0.0.0-20230828082145-3c4c8a2d2371 // indirect
	github.com/bmatcuk/doublestar v1.3.4 // indirect
	github.com/cloudflare/circl v1.3.3 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.4 // indirect
	github.com/cyphar/filepath-securejoin v0.2.4 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/go-git/gcfg v1.5.1-0.20230307220236-3a3c6141e376 // indirect
	github.com/go-git/go-billy/v5 v5.5.0 // indirect
	github.com/go-git/go-git/v5 v5.11.0 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99 // indirect
	github.com/kevinburke/ssh_config v1.2.0 // indirect
	github.com/otiai10/copy v1.11.0 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/pjbgf/sha1cd v0.3.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sergi/go-diff v1.3.1 // indirect
	github.com/skeema/knownhosts v1.2.1 // indirect
	github.com/spiegel-im-spiegel/errs v1.0.5 // indirect
	github.com/xanzy/ssh-agent v0.3.3 // indirect
	github.com/xrash/smetrics v0.0.0-20240312152122-5f08fbb34913 // indirect
	gitlab.com/gitlab-org/security-products/analyzers/report/v4 v4.4.0 // indirect
	gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2 v2.0.8 // indirect
	gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3 v3.0.0 // indirect
	golang.org/x/crypto v0.22.0 // indirect
	golang.org/x/mod v0.17.0 // indirect
	golang.org/x/net v0.24.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/tools v0.20.0 // indirect
	gopkg.in/warnings.v0 v0.1.2 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

go 1.19
