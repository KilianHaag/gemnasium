ARG DS_PYTHON_VERSION=3.11

FROM golang:1.22-alpine AS build

ENV CGO_ENABLED=0

WORKDIR /go/src/app

COPY go.mod go.sum ./
RUN go mod download

COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go version && \
    go build -ldflags="-X '$PATH_TO_MODULE/cmd/gemnasium-python/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer \
    cmd/gemnasium-python/main.go

FROM python:$DS_PYTHON_VERSION-slim

COPY vrange/python /vrange/python
ENV VRANGE_DIR="/vrange"

ARG GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_WEB_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db"
ARG GEMNASIUM_DB_REF_NAME="master"

ENV GEMNASIUM_DB_LOCAL_PATH $GEMNASIUM_DB_LOCAL_PATH
ENV GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_REMOTE_URL
ENV GEMNASIUM_DB_WEB_URL $GEMNASIUM_DB_WEB_URL
ENV GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REF_NAME

ARG DS_GET_PIP_PATH="/get-pip.py"
ENV DS_GET_PIP_PATH $DS_GET_PIP_PATH

ARG PIPENV_DEFAULT_PYTHON_VERSION="3"
ENV PIPENV_DEFAULT_PYTHON_VERSION $PIPENV_DEFAULT_PYTHON_VERSION

ADD build/gemnasium-python/get-pip.py build/gemnasium-python/requirements.txt /

RUN \
    # include gcc build env, and upgrade packages
    apt-get update && \
    apt-get install -y wget git build-essential libssl-dev libffi-dev musl-dev python3-dev && \
    apt-get upgrade -y && \
    echo "deb https://deb.debian.org/debian unstable main" | tee -a /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -t unstable -y perl zlib1g && \
    apt-get remove -y \
    # Remove Perl package vulnerable to CVE-2023-31484
    # See https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/merge_requests/581#note_1583260404 for more details
    libperl5.36 perl-modules-5.36 \
    # Remove unused libaom3 vulnerable to CVE-2023-6879
    libaom3 && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* && \
    \
    # install pipenv and its requirements
    pip install -r /requirements.txt && \
    \
    # install vrange/python
    DIR=${PWD} && cd ${VRANGE_DIR}/python && \
    # PIPENV_VENV_IN_PROJECT=1 forces the virtualenv to be created
    # in the `/vrange/python` directory. This allows the command in
    # vrange/python.go to find and re-use the pre-installed dependencies.
    PIPENV_VENV_IN_PROJECT=1 pipenv sync --python python3 --verbose && cd ${DIR} && \
    \
    # clone gemnasium-db
    git clone --branch $GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_LOCAL_PATH && \
    \
    # give write access to CA certificates (OpenShift)
    mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-certificates.crt && \
    chmod g+w /etc/ssl/certs/ca-certificates.crt && \
    \
    # give write access to vulnerability database (OpenShift)
    chmod -R g+w $GEMNASIUM_DB_LOCAL_PATH && \
    # Enable OpenSSH to find relevant known_hosts files in the original home directory, and in the OpenShift compatible home directory we use.
    # See https://gitlab.com/gitlab-org/gitlab/-/issues/374571
    mkdir -p /root/.ssh && touch /root/.ssh/config && chmod 600 /root/.ssh/config && \
    echo 'UserKnownHostsFile /root/.ssh/known_hosts /tmp/.ssh/known_hosts' >> /root/.ssh/config && \
    echo "done"

# set user HOME to a directory where any user can write (OpenShift)
ENV HOME "/tmp"

COPY --from=build /analyzer /analyzer
ENTRYPOINT []
CMD ["/analyzer", "run"]
