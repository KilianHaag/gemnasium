libraryDependencies ++= {
  sbtVersion.value match {

    // sbt versions below 1.4 are not bundled in the image and don't support offline environments.
    // These versions are not compatible with the `sbt-dependency-tree` plugin so we install the deprecated `sbt-dependency-graph` plugin
    // NB: sbt 1.0.x is no longer supported as of GitLab 17.0 but it doesn't cost us to leave it here
    case version if (version.matches("^(1.0|1.1|1.2|1.3).*")) =>
      Seq(
        "net.virtual-void" % "sbt-dependency-graph" % "0.10.0-RC1"
        )

    // sbt 1.6.2 is the default version that we bundle in the image and it must support offline environments.
    // We install the `sbt-dependency-tree` plugin and the compatible compiler-bridge version that allows to
    // support offline environments by providing the necessary build dependencies locally.
    case version if (version.matches("^(1.6.2).*")) =>
      Seq(
        // The compiler-bridge is a tool that sits between sbt and the Scala
        // compiler. It lets sbt work with any Scala version by adjusting itself
        // based on the project's Scala version.
        "org.scala-sbt" % "compiler-bridge_2.12" % "1.6.0", // main artifact
        "org.scala-sbt" % "compiler-bridge_2.12" % "1.6.0" classifier "sources", // sources jar
        // We use version 1.6.2 of `sbt-dependency-tree` plugin as it is compatible with both sbt 1.6.2 and 1.9.7 which are the versions we bundled in the image.
        "org.scala-sbt" % "sbt-dependency-tree_2.12_1.0" % "1.6.2"
      )

    // sbt 1.9.7 is the only version we officially support for JAVA 21 so we also bundle it in the image and it must support offline environments.
    // So, for all other versions of sbt we install the `sbt-dependency-tree` plugin and the compiler-bridge version that is compatible with sbt 1.9.7
    // This allows to support JAVA 21 offline environments by providing the necessary build dependencies locally for 1.9.7 and it doesn't impact other versions
    // of sbt that are not supported in offline environements since they will be fetching their necessary dependencies at runtime.
    case _ =>
      Seq(
        "org.scala-sbt" % "compiler-bridge_2.12" % "1.9.5", // main artifact
        "org.scala-sbt" % "compiler-bridge_2.12" % "1.9.5" classifier "sources", // sources jar
        // We use version 1.6.2 of `sbt-dependency-tree` plugin as it is compatible with both sbt 1.6.2 and 1.9.7 which are the versions we bundled in the image.
        "org.scala-sbt" % "sbt-dependency-tree_2.12_1.0" % "1.6.2"
      )
  }
}