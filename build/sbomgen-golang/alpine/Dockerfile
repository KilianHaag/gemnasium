FROM golang:1.22-alpine AS sbomgen-build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY go.mod go.sum ./
RUN go mod download
COPY . .

# build the sbomgen binary and automatically set the Version
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    go version && \
    go build -ldflags="-X 'main.Version=$CHANGELOG_VERSION'" -o /sbomgen-golang \
    cmd/sbomgen-golang/main.go

FROM alpine:3.19

# set user HOME to a directory where any user can write (OpenShift)
ENV HOME "/tmp"

# download go toolchain required for determining the final list of modules used in a Go project
# via the golang.org/x/tools/go/packages package.
RUN apk add -q --no-progress --no-cache "go=~1.21"

COPY --from=sbomgen-build /sbomgen-golang /usr/local/bin/sbomgen-golang

ENTRYPOINT []
CMD ["sbomgen-golang", "run"]
