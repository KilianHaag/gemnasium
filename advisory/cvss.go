package advisory

import (
	cvss3 "github.com/spiegel-im-spiegel/go-cvss/v3"
	cvss2 "github.com/umisama/go-cvss"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v5"
)

// Severity parses the given cvss vectors and returns the report.SeverityLevel value
func (a Advisory) Severity() report.SeverityLevel {
	// attempt cvss3 first
	if a.CVSSV3 != "" {
		m := cvss3.New()

		err := m.ImportBaseVector(a.CVSSV3)
		if err == nil {
			return cvss3ScoreToSeverity(m.Base.Score())
		}
		// if an error is returned, we'll disregard it and try parsing the cvss2 vector next
	}

	// fallback to cvss2
	v, err := cvss2.ParseVectors(parenthesizeCVSS2(a.CVSSV2))
	if err != nil {
		return report.SeverityLevelUnknown
	}

	return cvss2ScoreToSeverity(v.BaseScore())
}

// cvss3ScoreToSeverity uses the CVSS v3.1 "Qualitative Severity Rating Scale" to convert the score to a
// severity level.
// See https://www.first.org/cvss/v3.1/specification-document#Qualitative-Severity-Rating-Scale for more details
func cvss3ScoreToSeverity(score float64) report.SeverityLevel {
	switch true {
	case score == 0:
		return report.SeverityLevelInfo
	case score > 0 && score < 4.0:
		return report.SeverityLevelLow
	case score >= 4.0 && score < 7.0:
		return report.SeverityLevelMedium
	case score >= 7.0 && score < 9.0:
		return report.SeverityLevelHigh
	case score >= 9.0:
		return report.SeverityLevelCritical
	default:
		return report.SeverityLevelUnknown
	}
}

// cvss2ScoreToSeverity uses the CVSS v2.0 Ratings scale to convert the score to a severity level.
// See https://nvd.nist.gov/vuln-metrics/cvss for more details
func cvss2ScoreToSeverity(score float64) report.SeverityLevel {
	switch true {
	case score >= 0 && score < 4.0:
		return report.SeverityLevelLow
	case score >= 4.0 && score < 7:
		return report.SeverityLevelMedium
	case score >= 7.0:
		return report.SeverityLevelHigh
	default:
		return report.SeverityLevelUnknown
	}
}

// CVSS2 vectors must be enclosed in parentheses
func parenthesizeCVSS2(inCVSS2 string) string {
	if inCVSS2 == "" {
		return ""
	}

	if inCVSS2[0:1] != "(" {
		inCVSS2 = "(" + inCVSS2
	}

	if inCVSS2[len(inCVSS2)-1:] != ")" {
		inCVSS2 = inCVSS2 + ")"
	}

	return inCVSS2
}
