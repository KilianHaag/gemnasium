package gradle

// Dependency from Gradle htmlDependencyReport task
type Dependency struct {
	Module          string       `json:"module"`
	Name            string       `json:"name"`
	Resolvable      string       `json:"resolvable"`
	HasConflict     bool         `json:"hasConflict"`
	AlreadyRendered bool         `json:"alreadyRendered"`
	Children        []Dependency `json:"children"`
}

// Insight from Gradle htmlDependencyReport task
type Insight struct {
	Module  string       `json:"module"`
	Insight []Dependency `json:"insight"`
}

// Configuration from Gradle htmlDependencyReport task
type Configuration struct {
	Name           string       `json:"name"`
	Description    string       `json:"description"`
	Dependencies   []Dependency `json:"dependencies"`
	ModuleInsights []Insight    `json:"moduleInsights"`
}

// Project from Gradle htmlDependencyReport task
type Project struct {
	Name           string          `json:"name"`
	Description    string          `json:"description"`
	Configurations []Configuration `json:"configurations"`
}

// Report from Gradle htmlDependencyReport task
type Report struct {
	Version        string  `json:"gradleVersion"`
	GenerationDate string  `json:"generationDate"`
	Project        Project `json:"project"`
}
