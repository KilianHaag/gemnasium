package pipdeptree

import (
	"encoding/json"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
)

// Package represents a python package including package name and version
type Package struct {
	Name    string `json:"package_name"`
	Key     string `json:"key"`
	Version string `json:"installed_version"` // Installed version
}

// Parse scans the JSON output of pipdeptree and returns a list of packages
func Parse(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	document := []struct {
		Package `json:"package"`
	}{}
	err := json.NewDecoder(r).Decode(&document)
	if err != nil {
		return nil, nil, err
	}

	result := make([]parser.Package, len(document))
	for i, pkg := range document {
		result[i] = parser.Package{Name: pkg.Name, Version: pkg.Version}
	}
	return result, nil, nil
}

func init() {
	parser.Register("pipdeptree", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypePypi,
		Filenames:   []string{"pipdeptree.json"},
	})
}
