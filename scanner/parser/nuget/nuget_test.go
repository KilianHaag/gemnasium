package nuget

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/testutil"
)

func TestNuget(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		t.Run("wrong version", func(t *testing.T) {
			fixture := testutil.Fixture(t, "wrong_version", "packages.lock.json")
			_, _, err := Parse(fixture, parser.Options{})
			require.EqualError(t, err, parser.ErrWrongFileFormatVersion.Error())
		})

		tests := map[string]struct {
			fixtureDir     string
			expectationDir string
		}{
			"web.api": {
				fixtureDir:     "web.api",
				expectationDir: "web.api",
			},
			"duplicates": {
				fixtureDir:     "duplicates",
				expectationDir: "duplicates",
			},
			"MainProject": {
				fixtureDir:     "MainProject",
				expectationDir: "MainProject",
			},
			"MainProject_v2": {
				fixtureDir:     "MainProject_v2",
				expectationDir: "MainProject",
			},
			"projectrefs": {
				fixtureDir:     "projectrefs",
				expectationDir: "projectrefs",
			},
		}
		for name, tc := range tests {
			t.Run(name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc.fixtureDir, "packages.lock.json")
				pkgs, deps, err := Parse(fixture, parser.Options{})
				require.NoError(t, err)

				t.Run("packages", func(t *testing.T) {
					testutil.RequireExpectedPackages(t, tc.expectationDir, pkgs)
				})

				t.Run("dependencies", func(t *testing.T) {
					testutil.RequireExpectedDependencies(t, tc.expectationDir, deps)
				})
			})
		}
	})

	t.Run("Invalid document", func(t *testing.T) {
		r := strings.NewReader("this should cause an error")
		pkgs, deps, err := Parse(r, parser.Options{})
		require.True(t, strings.Contains(err.Error(), "invalid character"))
		require.Empty(t, pkgs)
		require.Empty(t, deps)
	})
}
