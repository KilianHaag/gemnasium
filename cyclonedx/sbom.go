package cyclonedx

// Component contains the information about the software dependency
// (package details, version, etc.).
type Component struct {
	Name    string `json:"name,omitempty"`
	Version string `json:"version,omitempty"`
	PURL    string `json:"purl,omitempty"`
	// have to use componentType here because `type` is a reserved name
	ComponentType string `json:"type,omitempty"`
	BomRef        string `json:"bom-ref,omitempty"`
}

// Dependency lists the dependencies of a component.
type Dependency struct {
	// Ref is the BomRef of the dependent.
	Ref string `json:"ref,omitempty"`

	// DependsOn contains the BomRef of each dependency.
	DependsOn []string `json:"dependsOn,omitempty"`
}

// MetadataProperty is a lightweight name-value pair
type MetadataProperty struct {
	Name  string `json:"name,omitempty"`
	Value string `json:"value,omitempty"`
}

// Author is The person(s) who created the BOM
type Author struct {
	Name  string `json:"name,omitempty"`
	Email string `json:"email,omitempty"`
}

// Tool represents the tool(s) used in the creation of the BOM
type Tool struct {
	Vendor  string `json:"vendor,omitempty"`
	Name    string `json:"name,omitempty"`
	Version string `json:"version,omitempty"`
}

// Metadata is a BOM Metadata Object
type Metadata struct {
	Timestamp  string             `json:"timestamp,omitempty"`
	Tools      []Tool             `json:"tools,omitempty"`
	Authors    []Author           `json:"authors,omitempty"`
	Properties []MetadataProperty `json:"properties,omitempty"`
}

// SBOM represents the CycloneDX software bill of materials
type SBOM struct {
	BomFormat    string       `json:"bomFormat,omitempty"`
	SpecVersion  string       `json:"specVersion,omitempty"`
	SerialNumber string       `json:"serialNumber,omitempty"`
	Version      int          `json:"version,omitempty"`
	Metadata     Metadata     `json:"metadata,omitempty"`
	Components   []Component  `json:"components,omitempty"`
	Dependencies []Dependency `json:"dependencies,omitempty"`
	// the following fields are used as metadata properties
	// and/or used internally to pass information
	// InputFilePath is the relative path to the file that generated this SBOM
	InputFilePath string `json:"-"`
	// OutputFilePath is the relative path to the SBOM file itself
	OutputFilePath string `json:"-"`
	PackageManager string `json:"-"`
	PackageType    string `json:"-"`
}
