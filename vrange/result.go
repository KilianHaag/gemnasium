package vrange

// ResultSet contains queries and the result of these.
type ResultSet map[Query]result

type result struct {
	satisfies bool  // satifised tells if the version is in range
	err       error // err is an error raised when evaluating the version or the range
}

// Satisfies retrieves the result for a given query.
// It tells if the version is in the range range.
func (set ResultSet) Satisfies(q Query) (bool, error) {
	if result, ok := set[q]; ok {
		return result.satisfies, result.err
	}
	return false, ErrQueryNotFound{q}
}

// Set is used by resolver to store the result of a given query.
func (set *ResultSet) Set(q Query, v bool, err error) {
	(*set)[q] = result{v, err}
}
