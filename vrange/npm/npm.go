package npm

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/cli"

func init() {
	cli.Register("npm", "npm/rangecheck.js")
	cli.Register("conan", "npm/rangecheck.js")
}
